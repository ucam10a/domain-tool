package com.yung.app;

import java.awt.BorderLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import javax.swing.JTextField;
import javax.swing.JLabel;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;

/**
 * Swing execute tool
 * 
 * @author Yung-Long Li
 *
 */
public class SwingApp extends JPanel implements ActionListener {
    
    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    public static final String newline = System.getProperty("line.separator");
    JButton exeButton, domainButton, harButton;
    JTextField arg1, arg2, arg3, arg4;
    JLabel lab1, lab2, lab3, lab4;
    private JTextArea log;
    private JScrollPane logScrollPane;
    private List<RunObject> runObjList = new ArrayList<RunObject>();
    private Properties prop;
    private static String currentDir = "";
    public static final String HAR_FILE_PATH = "harFilePath";
    public static final String DOMAIN_FILE_PATH = "domainFilePath";
    
    public SwingApp(RunObject... runObjs) {
        
        super(new BorderLayout());

        if (runObjs != null) {
            for (RunObject runObj : runObjs) {
                runObjList.add(runObj);
            }
        }
        log = new JTextArea(30, 30);
        log.setFont(log.getFont().deriveFont(20f));
        log.setMargin(new Insets(5, 5, 5, 5));
        log.setEditable(false);
        logScrollPane = new JScrollPane(log);
        
        exeButton = new JButton("run", null);
        exeButton.addActionListener(this);
        domainButton = new JButton("domain", null);
        domainButton.addActionListener(this);
        harButton = new JButton("har", null);
        harButton.addActionListener(this);
        
        lab1 = new JLabel("arg1");
        arg1 = new JTextField("", 8);
        lab2 = new JLabel("arg2");
        arg2 = new JTextField("", 8);
        lab3 = new JLabel("arg3");
        arg3 = new JTextField("", 8);
        lab4 = new JLabel("arg4");
        arg4 = new JTextField("", 8);
        
        JPanel buttonPanel = new JPanel();
        /*
        buttonPanel.add(lab1);
        buttonPanel.add(arg1);
        buttonPanel.add(lab2);
        buttonPanel.add(arg2);
        buttonPanel.add(lab3);
        buttonPanel.add(arg3);
        buttonPanel.add(lab4);
        buttonPanel.add(arg4);
        */
        buttonPanel.add(harButton);
        buttonPanel.add(domainButton);
        buttonPanel.add(exeButton);
        
        //Add the buttons and the log to this panel.
        add(buttonPanel, BorderLayout.PAGE_START);
        add(logScrollPane, BorderLayout.CENTER);
    
    }

    public void exe(String[] args, Properties prop, JScrollPane logScrollPane, JTextArea log) throws Exception{
        for (RunObject runObj : runObjList) {
            runObj.run(args, prop, logScrollPane, log);
        }
    }
    
    public void actionPerformed(ActionEvent e) {
        
        if (e.getSource() == exeButton) {
            log.setText("");
            AbstractRun.setSTOP(false);
            ExecutorService executorService = Executors.newScheduledThreadPool(1);
            executorService.execute(new Runnable() {
                public void run() {
                    try {
                        String[] args = new String[4];
                        args[0] = arg1.getText();
                        args[1] = arg2.getText();
                        args[2] = arg3.getText();
                        args[3] = arg4.getText();
                        exe(args, prop, logScrollPane, log);
                    } catch (Exception ex) {
                        log.append("Run fail!" + newline + ex.toString() + newline);
                        ex.printStackTrace();
                    }
                }
            });
            executorService.shutdown();
        }
        if (e.getSource() == domainButton) {
            JFileChooser fc = new JFileChooser();
            String temp = new File(currentDir).getAbsolutePath();
            File dir = new File(temp);
            fc.setCurrentDirectory(dir);
            int returnVal = fc.showOpenDialog(null);
            if (returnVal == JFileChooser.APPROVE_OPTION) {
                File f = fc.getSelectedFile();
                if (prop == null) prop = new Properties();
                prop.put(DOMAIN_FILE_PATH, f.getAbsolutePath());
                currentDir = f.getParent();
            }
        }
        if (e.getSource() == harButton) {
            JFileChooser fc = new JFileChooser();
            String temp = new File(currentDir).getAbsolutePath();
            File dir = new File(temp);
            fc.setCurrentDirectory(dir);
            int returnVal = fc.showOpenDialog(null);
            if (returnVal == JFileChooser.APPROVE_OPTION) {
                File f = fc.getSelectedFile();
                if (!f.getName().endsWith(".har") && !f.getName().endsWith(".HAR")) {
                    log.append("Error: file is not har file, please select har file" + newline);
                } else {
                    if (prop == null) prop = new Properties();
                    prop.put(HAR_FILE_PATH, f.getAbsolutePath());
                    currentDir = f.getParent();
                }
            }
        }
        
    }
    
    private void createAndShowGUI() {
        //Create and set up the window.
        JFrame frame = new JFrame("App");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        //Add content to the window.
        frame.add(this);

        //Display the window.
        frame.pack();
        frame.setVisible(true);
        
        frame.addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                // do something
            }
        });
        
    }

    public void run() {
        //Schedule a job for the event dispatch thread:
        //creating and showing this application's GUI.
        SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                //Turn off metal's use of bold fonts
                UIManager.put("swing.boldMetal", Boolean.FALSE);
                createAndShowGUI();
            }
        });
    }
    
    public static void main(String[] args) {
        
        SwingApp app = new SwingApp(
            new RunObject() {
                public void run(String[] args, Properties prop, JScrollPane logScrollPane, JTextArea log) throws Exception {
                    log.append("Hello! " + args[0] + newline);
                }
            }
        );
        app.run();
        
    }
}