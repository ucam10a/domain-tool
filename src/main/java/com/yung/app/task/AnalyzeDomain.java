package com.yung.app.task;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.HashSet;
import java.util.Properties;
import java.util.Set;

import javax.swing.JScrollPane;
import javax.swing.JTextArea;

import com.yung.app.RunObject;
import com.yung.app.SwingApp;
import com.yung.tool.HarTool;

public class AnalyzeDomain implements RunObject {

    public void run(String[] args, Properties prop, JScrollPane logScrollPane, JTextArea log) throws Exception {
        
        Set<String> allowSet = new HashSet<String>();
        String domainPath = prop.getProperty(SwingApp.DOMAIN_FILE_PATH);
        if (domainPath != null && !"".equals(domainPath)) {
            BufferedReader br = new BufferedReader(new FileReader(new File(domainPath)));
            try {
                String line = br.readLine();
                while (line != null) {
                    if (line != null && !line.equals("")) allowSet.add(line);
                    line = br.readLine();
                }
            } finally {
                br.close();
            }
        }
        
        String path = prop.getProperty(SwingApp.HAR_FILE_PATH);
        if (path == null || "".equals(path)) {
            log.append("Error: please select har file first!" + SwingApp.newline);
            return;
        }
        File harFile = new File(path);
        Set<String> filters = new HashSet<String>();
        for (String arg : args) {
            if (arg != null && !"".equals(arg)) {
                filters.add(arg);
            }
        }
        Set<String> domainSet = new HashSet<String>(); 
        if (filters.size() == 0) {
            Set<String> set = HarTool.extractDomain(harFile);
            domainSet.addAll(set);
        } else {
            Set<String> set = HarTool.extractDomain(harFile, filters);
            domainSet.addAll(set);
        }
        for (String domain : domainSet) {
            if (allowSet.size() > 0) {
                if (allowSet.contains(domain)) {
                    log.append(domain + SwingApp.newline);
                } else {
                    log.append(domain + " , not in list!" + SwingApp.newline);
                }
            } else {
                log.append(domain + SwingApp.newline);
            }
        }
        
    }

}
