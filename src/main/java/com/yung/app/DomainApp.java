package com.yung.app;

import com.yung.app.task.AnalyzeDomain;

public class DomainApp {

    public static void main(String[] args) {
        
        SwingApp app = new SwingApp(
            new AnalyzeDomain()        
        );
        app.run();
        
    }

}
