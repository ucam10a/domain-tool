package com.yung.tool;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

public class HarTool {

    public static Map<String, Object> parseHar(File harFile) throws IOException {
        Gson gson = new Gson();
        StringBuilder sb = new StringBuilder();
        BufferedReader br = new BufferedReader(new FileReader(harFile));
        try {
            String line = br.readLine();
            while (line != null) {
                if (line != null && !line.equals("")) {
                    line = line.trim();
                    sb.append(line);
                }
                line = br.readLine();
            }
        } finally {
            br.close();
        }
        String json = sb.toString();
        Type mapType = new TypeToken<Map<String, Object>>() {}.getType();
        Map<String, Object> map = gson.fromJson(json, mapType);
        return map;
    }
    
    @SuppressWarnings("unchecked")
    public static Set<String> parseMultipartUrls(File harFile) throws IOException {
        Set<String> ret = new HashSet<String>();
        Map<String, Object> map = parseHar(harFile);
        if (map != null && map.size() > 0) {
            Map<String, Object> logMap = (Map<String, Object>) map.get("log");
            if (logMap != null && logMap.size() > 0) {
                List<Map<String, Object>> entries = (List<Map<String, Object>>) logMap.get("entries");
                if (entries != null && entries.size() > 0) {
                    for (Map<String, Object> entry : entries) {
                        Map<String, Object> reqMap = (Map<String, Object>) entry.get("request");
                        List<Map<String, Object>> headers = (List<Map<String, Object>>) reqMap.get("headers");
                        if (headers != null && headers.size() > 0) {
                            for (Map<String, Object> headMap : headers) {
                                String name = (String) headMap.get("name");
                                if (name != null && !"".equals(name)) {
                                    if (name.equals("Content-Type")) {
                                        String val = (String) headMap.get("value");
                                        if (val != null && !"".equals(val) && val.toLowerCase().contains("multipart/form-data")) {
                                            String url = (String) reqMap.get("url");
                                            if (url != null && !"".equals(url)) {
                                                ret.add(url);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        return ret;
    }
    
    public static List<Status> extractStatus(File harFile) throws IOException {
        List<Status> ret = new ArrayList<Status>();
        BufferedReader br = new BufferedReader(new FileReader(harFile));
        try {
            String prevLine = null;
            String line = br.readLine();
            boolean start = true;
            Status status = null;
            while (line != null) {
                if (start) {
                    status = new Status();
                }
                if (line != null && !line.equals("")) {
                    line = line.trim();
                    if (line.startsWith("\"url\":")) {
                        if (prevLine != null && prevLine.startsWith("\"method\":")) {
                            String domain = getDomain(line);
                            if (domain != null && !"".equals(domain)) {
                                if (status.getUrl() == null) {
                                    status.setUrl(domain);
                                } else {
                                    ret.add(status);
                                    status = new Status();
                                }
                                start = false;
                            }
                        }
                    } else if (line.startsWith("\"status\":")) {
                        String statusCd = getStatus(line);
                        if (statusCd != null && !"".equals(statusCd)) {
                            status.setStatus(Integer.valueOf(statusCd));
                        }
                        ret.add(status);
                        start = true;
                    }
                }
                prevLine = line;
                line = br.readLine();
            }
        } finally {
            br.close();
        }
        return ret;
    }
    
    public static Set<String> extractDomain(File harFile) throws IOException {
        Set<String> ret = new HashSet<String>();
        BufferedReader br = new BufferedReader(new FileReader(harFile));
        try {
            String line = br.readLine();
            while (line != null) {
                if (line != null && !line.equals("")) {
                    line = line.trim();
                    if (line.startsWith("\"url\":")) {
                        String domain = getDomain(line);
                        if (domain != null && !"".equals(domain)) {
                            ret.add(domain);
                        }
                    }
                }
                line = br.readLine();
            }
        } finally {
            br.close();
        }
        return ret;
    }
    
    public static Set<String> extractDomain(File harFile, Set<String> filters) throws IOException {
        Set<String> temp = extractDomain(harFile);
        Set<String> ret = new HashSet<String>();
        for (String domain : temp) {
            for (String filter : filters) {
                if (domain.indexOf(filter) >= 0) {
                    ret.add(domain);
                    break;
                }
            }
        }
        return ret;
    }

    private static String getDomain(String line) {
        String ret = line.substring(6);
        String protocol = "http://";
        int idx = ret.indexOf(protocol);
        if (idx < 0) {
            protocol = "https://";
            idx = ret.indexOf(protocol);
        }
        if (idx < 0) {
            return null;
        }
        ret = ret.substring(idx + protocol.length());
        idx = ret.indexOf("/");
        if (idx < 0) {
            return null;
        }
        ret = ret.substring(0, idx);
        return ret;
    }
    
    private static String getStatus(String line) {
        int preFix = "\"status\":".length();
        String ret = line.substring(preFix, line.length() - 1);
        if (ret != null) {
            ret = ret.trim();
        }
        return ret;
    }
    
    public static void main(String args[]) throws Exception {
        
        File harFile = new File("E:/test/scp.har");
        List<Status> statusList = extractStatus(harFile);
        
        for (Status status : statusList) {
            System.out.println(status.getUrl() + ":" + status.getStatus());
        }
        
    }
    
}
